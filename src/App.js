import { BrowserRouter, Route, Routes } from "react-router-dom";
import "./index.css";
import Login from "./components/Login/Login";
import SignUp from "./components/SignUp/SignUp";
import AdminDashboard from "./Dashboard/AdminDashboard/AdminDashboard";
import UserDashboard from "./Dashboard/UserDashboard/UserDashboard";
import CreateCase from "./Dashboard/AdminDashboard/CreateCase/CreateCase";

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Routes>
          <Route path="/" element={<Login />} />
          <Route path="/signup" element={<SignUp />} />
          <Route path="/admindashboard" element={<AdminDashboard />} />
          <Route path="/userdashboard" element={<UserDashboard />} />
          <Route path="/addCase" element={<CreateCase />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;

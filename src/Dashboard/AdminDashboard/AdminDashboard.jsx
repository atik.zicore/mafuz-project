import React from "react";
import { ReactTyped } from "react-typed";
import AllCaseSummery from "./AllCaseSummery/AllCaseSummery";
import UserHeader from "../../components/Shared/Header/UserHeader";

const AdminDashboard = () => {
  return (
    <div className="w-full h-full ">
      <UserHeader />
      <div className="text-center font-bold py-4 md:py-8 lg:py-12 ">
        <ReactTyped
          strings={["RFID Admin Dashboard"]}
          typeSpeed={200}
          className="text-lg md:text-2xl lg:text-7xl text-emerald-500"
          loop
        />
      </div>

      <div className="text-black">
        <AllCaseSummery />
      </div>
    </div>
  );
};

export default AdminDashboard;

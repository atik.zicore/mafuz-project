import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import AdminHeader from "../../../components/Shared/Header/AdminHeader";

const CreateCase = () => {
  const [vicName, setVicName] = useState("");
  const [vicEmail, setVicEmail] = useState("");
  const [vicMobile, setVicMobile] = useState("");
  const [vicCarNumber, setVicCarNumber] = useState("");
  const [vicReason, setVicReason] = useState("");
  const baseUrl = "https://6648ccae4032b1331bec78f7.mockapi.io/allCaseFile";
  const navigate = useNavigate();

  const formData = { vicName, vicEmail, vicMobile, vicCarNumber, vicReason };

  const handleCaseSubmit = async (e) => {
    e.preventDefault();

    try {
      const response = await fetch(baseUrl, {
        method: "POST",
        headers: {
          "content-type": "application/json",
        },
        body: JSON.stringify(formData),
      });
      await response.json();
      setVicName("");
      setVicEmail("");
      setVicMobile("");
      setVicCarNumber("");
      setVicReason("");

    //   console.log(formData);
      alert("Case added successfully");
      //   <navigate to='/admindashboard' />
      navigate("/admindashboard"); // Correct usage of navigate function
    } catch (error) {
      alert("error on case creating", error);
    }
  };

  return (
    <>
    <AdminHeader/>
    <div className="w-full h-dvh bg-gradient-to-br from-teal-500 via-cyan-700 to-red-300  ">
      <div className="max-w-xl p-4 mx-auto">
        <form className="" onSubmit={handleCaseSubmit}>
          <div className="w-full mb-2 pt-8 space-y-2 sm:space-y-4 lg:space-y-8  ">
            <div className="relative z-0 p">
              <input
                type="text"
                id="victim_name"
                className="block py-2.5 px-0 w-full text-base text-white bg-transparent border-0 border-b-2 border-gray-300 appearance-none darkkk:text-white darkkk:border-gray-600 darkkk:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-secondary peer"
                placeholder=" "
                onChange={(e) => setVicName(e.target.value)}
                value={vicName}
                required
              />
              <label
                htmlFor="victim_name"
                className="absolute text-base text-black/50 darkkk:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:start-0 peer-focus:text-black peer-focus:darkkk:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6 rtl:peer-focus:translate-x-1/4 rtl:peer-focus:left-auto capitalize "
              >
                Enter Full Name
              </label>
            </div>
            <div className="relative z-0 p">
              <input
                type="email"
                id="victim_email"
                className="block py-2.5 px-0 w-full text-base text-white bg-transparent border-0 border-b-2 border-gray-300 appearance-none darkkk:text-white darkkk:border-gray-600 darkkk:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-secondary peer"
                placeholder=" "
                onChange={(e) => setVicEmail(e.target.value)}
                value={vicEmail}
                required
              />
              <label
                htmlFor="victim_email"
                className="absolute text-base text-black/50 darkkk:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:start-0 peer-focus:text-black peer-focus:darkkk:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6 rtl:peer-focus:translate-x-1/4 rtl:peer-focus:left-auto capitalize "
              >
                Enter email
              </label>
            </div>
            <div className="relative z-0 p">
              <input
                type="tel"
                id="victim_mobile"
                className="block py-2.5 px-0 w-full text-base text-white bg-transparent border-0 border-b-2 border-gray-300 appearance-none darkkk:text-white darkkk:border-gray-600 darkkk:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-secondary peer"
                placeholder=" "
                onChange={(e) => setVicMobile(e.target.value)}
                value={vicMobile}
                required
              />
              <label
                htmlFor="victim_mobile"
                className="absolute text-base text-black/50 darkkk:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:start-0 peer-focus:text-black peer-focus:darkkk:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6 rtl:peer-focus:translate-x-1/4 rtl:peer-focus:left-auto capitalize "
              >
                Mobile
              </label>
            </div>
            <div className="relative z-0 p">
              <input
                type="number"
                id="victim_car_number"
                className="block py-2.5 px-0 w-full text-base text-white bg-transparent border-0 border-b-2 border-gray-300 appearance-none darkkk:text-white darkkk:border-gray-600 darkkk:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-secondary peer"
                placeholder=" "
                onChange={(e) => setVicCarNumber(e.target.value)}
                value={vicCarNumber}
                required
              />
              <label
                htmlFor="victim_car_number"
                className="absolute text-base text-black/50 darkkk:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:start-0 peer-focus:text-black peer-focus:darkkk:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6 rtl:peer-focus:translate-x-1/4 rtl:peer-focus:left-auto capitalize "
              >
                RFID Number
              </label>
            </div>
            <div className="relative z-0 p">
              <input
                type="text"
                id="victim_reason"
                className="block py-2.5 px-0 w-full text-base text-white bg-transparent border-0 border-b-2 border-gray-300 appearance-none darkkk:text-white darkkk:border-gray-600 darkkk:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-secondary peer"
                placeholder=" "
                onChange={(e) => setVicReason(e.target.value)}
                value={vicReason}
                required
              />
              <label
                htmlFor="victim_reason"
                className="absolute text-base text-black/50 darkkk:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:start-0 peer-focus:text-black peer-focus:darkkk:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6 rtl:peer-focus:translate-x-1/4 rtl:peer-focus:left-auto capitalize "
              >
                Reason
              </label>
            </div>
            <div>
              <input
                type="submit"
                className="w-full bg-[#0A2441] rounded-lg py-2 text-white font-light  cursor-pointer"
                value="Submit Case"
              />
            </div>
          </div>
        </form>
      </div>
    </div>
    </>
  );
};

export default CreateCase;

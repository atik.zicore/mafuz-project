import axios from "axios";
import React, { Fragment, useEffect, useState } from "react";
import Cookies from "js-cookie";
import { Dialog, Transition } from "@headlessui/react";
import { toast } from "react-toastify";
import { useNavigate } from "react-router-dom";

const AllCaseSummery = () => {
  const navigate = useNavigate();
  const baseUrl = "http://54.199.157.33/";
  const authTokens = Cookies.get("access_token_user") || null;

  const [rfid, setRfid] = useState("");
  const [allData, setAllData] = useState([]);
  const [isOpen, setIsOpen] = useState(false);

  useEffect(() => {
    axios
      .get(baseUrl + "rfid/all/", {
        headers: {
          Authorization: `Bearer ${authTokens}`,
          "Content-Type": "application/json",
          accept: "application/json",
        },
      })
      .then((response) => {
        setAllData(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [authTokens]);

  const handleFormSubmit = async (e) => {
    e.preventDefault();
    const formData = { rfid };

    axios
      .post(baseUrl + "auth/rfid/create", formData, {
        headers: {
          Authorization: `Bearer ${authTokens}`,
          "Content-Type": "application/json",
          accept: "application/json",
        },
      })
      .then((response) => {
        toast.success("RFID number added successfully", {
          position: "top-right",
          autoClose: 2000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          theme: "light",
        });
        if (response.status === 200) {
          navigate("/admindashboard");
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const handleSendMail = (id_user) => {
    axios
      .post(
        baseUrl + `rfid/send_mail/${id_user}/`,
        {},
        {
          headers: {
            Authorization: `Bearer ${authTokens}`,
            "Content-Type": "application/json",
            accept: "application/json",
          },
        }
      )
      .then((response) => {
        console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <div className="w-full h-full">
      <div className="flex justify-end items-center pr-8 pb-5">
        <p
          onClick={() => setIsOpen(true)}
          className="px-4 py-2 rounded-md bg-teal-500 w-40 text-white font-semibold uppercase text-center cursor-pointer"
        >
          Add RFID
        </p>
      </div>
      <section className="w-full h-full overflow-x-auto px-4 md:px-8 lg:px-12">
        <table className="w-full border border-neutral-200 text-center text-sm font-light text-surface darkk:border-white/10 darkk:text-white">
          <thead className="border-b border-neutral-200 font-medium darkk:border-white/10">
            <tr className="capitalize">
              <th className="border-e border-neutral-200 px-6 py-4 darkk:border-white/10">
                SL
              </th>
              <th className="border-e border-neutral-200 px-6 py-4 darkk:border-white/10">
                Name
              </th>
              <th className="border-e border-neutral-200 px-6 py-4 darkk:border-white/10">
                RFID
              </th>
              <th className="border-e border-neutral-200 px-6 py-4 darkk:border-white/10">
                Date
              </th>
              <th className="border-e border-neutral-200 px-6 py-4 darkk:border-white/10">
                Time
              </th>
            </tr>
          </thead>
          <tbody>
            {allData.map((item, index) => (
              <tr
                className="border-b border-neutral-200 darkk:border-white/10"
                key={index}
              >
                <td className="whitespace-nowrap border-e border-neutral-200 px-6 py-4 font-medium darkk:border-white/10">
                  {item.id}
                </td>
                <td className="whitespace-nowrap border-e border-neutral-200 px-6 py-4 darkk:border-white/10">
                  {item.name}
                </td>
                <td className="whitespace-nowrap border-e border-neutral-200 px-6 py-4 darkk:border-white/10">
                  {item.rfid}
                </td>
                <td className="whitespace-nowrap border-e border-neutral-200 px-6 py-4 darkk:border-white/10">
                  {item.date}
                </td>
                <td className="whitespace-nowrap border-e border-neutral-200 px-6 py-4 darkk:border-white/10">
                  {item.time}
                </td>
                <td className="whitespace-nowrap border-e border-neutral-200 px-6 py-4 darkk:border-white/10 flex justify-center gap-4">
                  <button
                    type="button"
                    className="px-2 py-1 rounded-lg capitalize text-sm font-bold text-white bg-yellow-400"
                    onClick={() => handleSendMail(item.id)}
                  >
                    Pay Fine & Mail
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </section>
      <Transition appear show={isOpen} as={Fragment}>
        <Dialog
          as="div"
          className="relative z-10"
          onClose={() => setIsOpen(false)}
        >
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <div className="fixed inset-0 bg-black bg-opacity-60" />
          </Transition.Child>
          <div className="fixed inset-0 overflow-y-auto">
            <div className="flex min-h-full items-center justify-center p-4 text-center">
              <Transition.Child
                as={Fragment}
                enter="ease-out duration-300"
                enterFrom="opacity-0 scale-95"
                enterTo="opacity-100 scale-100"
                leave="ease-in duration-200"
                leaveFrom="opacity-100 scale-100"
                leaveTo="opacity-0 scale-95"
              >
                <Dialog.Panel className="w-full max-w-lg text-white transform overflow-hidden rounded-2xl bg-black p-6 align-middle shadow-xl transition-all">
                  <section className="max-w-3xl min-h-96 mx-auto flex justify-center items-center">
                    <form
                      className="w-full h-full justify-center items-center"
                      onSubmit={handleFormSubmit}
                    >
                      <div className="relative z-0 p">
                        <input
                          type="tel"
                          id="rfid_number"
                          className="block py-2.5 px-0 w-full text-base text-white bg-transparent border-0 border-b-2 border-gray-300 appearance-none focus:outline-none focus:ring-0 focus:border-secondary peer"
                          placeholder=" "
                          onChange={(e) => setRfid(e.target.value)}
                          value={rfid}
                          required
                        />
                        <label
                          htmlFor="rfid_number"
                          className="absolute text-base text-gray-500 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:start-0 peer-focus:text-secondary peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                        >
                          Enter RFID number
                        </label>
                      </div>
                      <input
                        type="submit"
                        className="w-full h-full bg-emerald-400 mt-4 py-2 rounded-lg"
                      />
                    </form>
                  </section>
                </Dialog.Panel>
              </Transition.Child>
            </div>
          </div>
        </Dialog>
      </Transition>
    </div>
  );
};

export default AllCaseSummery;

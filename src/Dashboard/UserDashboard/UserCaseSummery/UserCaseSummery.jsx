import axios from "axios";
import React, { Fragment, useEffect, useState } from "react";
import Cookies from "js-cookie";
import { Dialog, Transition } from "@headlessui/react";

const UserCaseSummery = () => {
  const baseUrl = "http://54.199.157.33/";
  const authTokens = Cookies.get("access_token_user") || null;

  const [allData, setAllData] = useState([]);
  useEffect(() => {
    axios
      .get(baseUrl + "rfid/all/", {
        headers: {
          Authorization: `Bearer ${authTokens}`,
          "Content-Type": "application/json",
          accept: "application/json",
        },
      })
      .then(function (response) {
        setAllData(response.data);
      })
      .catch(function (error) {
        console.log(error);
      });
  }, [authTokens]);

  const [isOpen, setIsOpen] = useState(false);

  return (
    <div className="w-full h-full space-y-4">
      <div className="flex justify-end items-center pr-8 disabled">
        <p className="disabled cursor-not-allowed px-4 py-2 rounded-md bg-gray-300 w-40 text-white font-semibold uppercase text-center">
          add rfid
        </p>
      </div>
      <section className="w-full h-full overflow-auto px-4 md:px-8 lg:px-12">
        <table className="w-full border border-neutral-200 text-center text-sm font-light text-surface darkk:border-white/10 darkk:text-white">
          <thead className="border-b border-neutral-200 font-medium darkk:border-white/10">
            <tr className="capitalize">
              <th
                scope="col"
                className="border-e border-neutral-200 px-6 py-4 darkk:border-white/10"
              >
                SL
              </th>
              <th
                scope="col"
                className="border-e border-neutral-200 px-6 py-4 darkk:border-white/10"
              >
                name
              </th>
              <th
                scope="col"
                className="border-e border-neutral-200 px-6 py-4 darkk:border-white/10"
              >
                rfid
              </th>
              <th
                scope="col"
                className="border-e border-neutral-200 px-6 py-4 darkk:border-white/10"
              >
                date
              </th>
              <th
                scope="col"
                className="border-e border-neutral-200 px-6 py-4 darkk:border-white/10"
              >
                time
              </th>
            </tr>
          </thead>
          <tbody>
            {allData?.map((item, index) => (
              <tr
                className="border-b border-neutral-200 darkk:border-white/10"
                key={index}
              >
                <td className="whitespace-nowrap border-e border-neutral-200 px-6 py-4 font-medium darkk:border-white/10">
                  {item.id}
                </td>
                <td className="whitespace-nowrap border-e border-neutral-200 px-6 py-4 darkk:border-white/10">
                  {item.name}
                </td>
                <td className="whitespace-nowrap border-e border-neutral-200 px-6 py-4 darkk:border-white/10">
                  {item.rfid}
                </td>
                <td className="whitespace-nowrap border-e border-neutral-200 px-6 py-4 darkk:border-white/10">
                  {item.date}
                </td>
                <td className="whitespace-nowrap border-e border-neutral-200 px-6 py-4 darkk:border-white/10">
                  {item.time}
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </section>

      {/* Modal */}
      <Transition appear show={isOpen} as={Fragment}>
        <Dialog as="div" className="relative z-10" onClose={() => setIsOpen(false)}>
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <div className="fixed inset-0 bg-black bg-opacity-60" />
          </Transition.Child>

          <div className="fixed inset-0 overflow-y-auto">
            <div className="flex min-h-full items-center justify-center p-4 text-center">
              <Transition.Child
                as={Fragment}
                enter="ease-out duration-300"
                enterFrom="opacity-0 scale-95"
                enterTo="opacity-100 scale-100"
                leave="ease-in duration-200"
                leaveFrom="opacity-100 scale-100"
                leaveTo="opacity-0 scale-95"
              >
                <Dialog.Panel className="w-full max-w-lg text-white transform overflow-hidden rounded-2xl bg-black p-6 align-middle shadow-xl transition-all">
                  <div>Form will be here</div>
                </Dialog.Panel>
              </Transition.Child>
            </div>
          </div>
        </Dialog>
      </Transition>
    </div>
  );
};

export default UserCaseSummery;

import React from "react";
import UserHeader from "../../components/Shared/Header/UserHeader";
import { ReactTyped } from "react-typed";
import UserCaseSummery from "./UserCaseSummery/UserCaseSummery";
// import UserCaseSummery from "../AdminDashboard/UserCaseSummery/UserCaseSummery";

const UserDashboard = () => {
  return (
    <div className="w-full h-full ">
      <UserHeader />
      <div className="text-center font-bold py-4 md:py-8 lg:py-12 ">
        <ReactTyped
          strings={["RFID User Dashboard"]}
          typeSpeed={200}
          className="text-lg md:text-2xl lg:text-7xl text-emerald-500"
          loop
        />
      </div>

      <div className="text-black">
        {/* <AllCaseSummery /> */}
        <UserCaseSummery />
      </div>
    </div>
  );
};

export default UserDashboard;

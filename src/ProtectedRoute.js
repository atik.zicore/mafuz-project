// // ProtectedRoute.js
// import React from 'react';
// import { Navigate } from 'react-router-dom';
// import { useAuth } from './AuthContext';

// const ProtectedRoute = ({ children, role }) => {
//   const { user } = useAuth();

//   if (!user) {
//     return <Navigate to="/" />;
//   }

//   if (role && user.role !== role) {
//     return <Navigate to="/" />;
//   }

//   return children;
// };

// export default ProtectedRoute;

import React from 'react';
import { Route, Navigate } from 'react-router-dom';

const ProtectedRoute = ({ role, children }) => {
  // Logic to check if user is logged in and has the correct role
  const isLoggedIn = true; // Placeholder for checking if user is logged in
  const userRole = 'admin'; // Placeholder for user role
  
  // Redirect based on role
  if (!isLoggedIn || userRole !== role) {
    return <Navigate to="/" />;
  }

  // Render the child components if user is logged in and has the correct role
  return <Route>{children}</Route>;
};

export default ProtectedRoute;


import React, { useState } from "react";
import { NavLink, useNavigate } from "react-router-dom";
import {  toast } from "react-toastify";
import axios from "axios";

const SignUpForm = () => {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [pass, setPass] = useState("");
  const navigate = useNavigate();

  const baseUrl = "http://54.199.157.33/auth/register";

  const handleFormSubmit = async (e) => {
    e.preventDefault();

    const formData = { name, email, password: pass };
    axios
      .post(baseUrl, formData)
      .then(function (response) {
        // console.log(response);
        toast.success(" Sign Up Successful", {
          position: "top-right",
          autoClose: 2000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          theme: "light",
        });
        if (response.status === 200) {
          navigate("/");
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  return (
    <div className="w-full xl:w-1/2 p-8 relative h-full">
      <h2 className="text-3xl text-center py-4">
        Sign Up with valid information
      </h2>

      <form
        className="w-full mb-2 pt-8 space-y-2 sm:space-y-4 lg:space-y-8"
        onSubmit={handleFormSubmit}
      >
        <div className="relative z-0 p">
          <input
            type="text"
            id="reg_name"
            className="block py-2.5 px-0 w-full text-base text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none focus:outline-none focus:ring-0 focus:border-secondary peer"
            placeholder=" "
            onChange={(e) => setName(e.target.value)}
            value={name}
            required
          />
          <label
            htmlFor="reg_name"
            className="absolute text-base text-gray-500 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-secondary peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
          >
            Enter Full Name
          </label>
        </div>
        <div className="relative z-0 p">
          <input
            type="email"
            id="reg_email"
            className="block py-2.5 px-0 w-full text-base text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none focus:outline-none focus:ring-0 focus:border-secondary peer"
            placeholder=" "
            onChange={(e) => setEmail(e.target.value)}
            value={email}
            required
          />
          <label
            htmlFor="reg_email"
            className="absolute text-base text-gray-500 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-secondary peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
          >
            Enter email address
          </label>
        </div>
        <div className="relative z-0 p">
          <input
            type="password"
            id="reg_pass"
            className="block py-2.5 px-0 w-full text-base text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none focus:outline-none focus:ring-0 focus:border-secondary peer"
            placeholder=" "
            onChange={(e) => setPass(e.target.value)}
            value={pass}
            required
          />
          <label
            htmlFor="reg_pass"
            className="absolute text-base text-gray-500 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-secondary peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
          >
            Enter Password
          </label>
        </div>

        <div>
          <input
            type="submit"
            className="w-full bg-[#0A2441] rounded-lg py-2 text-white font-light cursor-pointer"
          />
        </div>
      </form>

      <div className="pt-6">
        <p className="text-gray-500 text-base">
          Already have an account?{" "}
          <NavLink to="/" className="underline text-secondary capitalize">
            login here
          </NavLink>
        </p>
      </div>

    </div>
  );
};

export default SignUpForm;

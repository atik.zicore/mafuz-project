import React from 'react'
import SignUpForm from './SignUpForm';
import SignUpCard from './SignUpCard';

const SignUp = () => {
    const SignUpDoc = [
        {
          id: 1,
          title: "Red Light? Hit the Brakes!",
          article:
            "Think that red light means 'maybe stop'? Wrong! Running a red light can land you in hot water. Not only is it dangerous, but you could face a hefty fine or even lose your license. Play it safe, obey the light, and reach your destination happy - and legal.",
          image: "/Images/one.svg",
        },
        {
          id: 2,
          title: "Speeding? Slow Down, Save Money!",
          article:
            "Going a few miles over the limit might seem harmless, but speeding tickets add up. That extra cash could be better spent on gas, car washes, or even that cool accessory you've been eyeing. Plus, staying at the speed limit keeps you and everyone else safe. Why risk an accident and a bigger bill?",
          image: "/Images/two.svg",
        },
        {
          id: 3,
          title: "Eyes on the Road, Not Your Phone!",
          article:
            "A text can wait, a funny meme can wait, but a red light won't. Texting and driving is a recipe for disaster.  Distracted driving can lead to accidents, injuries, and even worse. Keep your eyes on the road, focus on driving, and avoid the temptation to multitask. Your phone can wait, your safety can't.",
          image: "/Images/three.svg",
        },
        {
          id: 4,
          title: "Seatbelts? Click It or Ticket!",
          article:
            "Seatbelts are your best friend in a car. They save lives.  It might seem like a hassle to buckle up for a short trip, but that's when accidents are most likely to happen.  Don't tempt fate. !",
          image: "/Images/two.svg",
        },
      ];
  return (
    <>
    <div className="w-full h-svh sm:h-dvh md:h-full flex justify-center items-center fixedbg py-4 ">
      <div className="bg-white shadow-lg relative  md:min-h-[40rem] flex justify-center items-center  rounded-lg w-full sm:max-w-[35rem] lg:max-w-[46rem] xl:max-w-5xl 2xl:max-w-7xl divide-x-2">
        <SignUpForm/>
        <SignUpCard data= {SignUpDoc}/>
      </div>
    </div>
  </>
  )
}

export default SignUp ;
import { useCallback, useEffect, useState } from "react";
import { MdKeyboardArrowLeft, MdKeyboardArrowRight } from "react-icons/md";

const LoggingCard = (props) => {
  const data = props.data;
  // console.log(data)

  const [currentIndex, setCurrentIndex] = useState(0);
  const [isPlaying, setIsPlaying] = useState(true);

  const handlePrev = () => {
    const isFirstSlide = currentIndex === 0;
    const newIndex = isFirstSlide ? data.length - 1 : currentIndex - 1;
    setCurrentIndex(newIndex);
  };



  const handleNext = useCallback(() => {
    setCurrentIndex((prevIndex) => {
      const nextIndex = prevIndex === data.length - 1 ? 0 : prevIndex + 1;
      // console.log(nextIndex); // Logging the updated index
      return nextIndex;
    });
  }, [data.length]);

  useEffect(() => {
    if (isPlaying) {
      const interval = setInterval(handleNext, 3000);
      return () => clearInterval(interval);
    }
  }, [isPlaying, handleNext]);


  const handleDotClick = (index) => {
    setCurrentIndex(index);
  };

  return (
    <>
      <div
        className="hidden xl:block lg:w-1/2 h-full relative  p-8 text-center "
        onMouseEnter={() => setIsPlaying(false)}
        onMouseLeave={() => setIsPlaying(true)}
      >
        {data.map((slide, index) => {
          return (

              <div
                className={`relative p-8 flex flex-col justify-center items-center h-[30rem] ${
                  index === currentIndex ? "block" : "hidden"
                }`}
                key={index}
              >
                <div className="mb-2">
                  <img
                    src={slide.image}
                    alt="demo pic"
                    style={{ width: 200, height: 200 }}
                  />
                </div>
                <h2 className="w-full text-xl py-2">{slide.title}</h2>
                <article>{slide.article}</article>
                <button className="absolute -left-4" onClick={handlePrev}>
                  <span>
                    <MdKeyboardArrowLeft className="h-10 w-10" />{" "}
                  </span>
                </button>
                <button className="absolute -right-4" onClick={handleNext}>
                  <span>
                    <MdKeyboardArrowRight className="h-10 w-10" />
                  </span>{" "}
                </button>
              </div>

          );
        })}

        <div className=" flex justify-center gap-4 items-center">
          {data.map((_, index) => {
            return (
              <div
                key={index}
                className={`w-6 h-6 rounded-full z-40 ${
                  index === currentIndex ? "bg-blue-500" : "bg-gray-400"
                }`}
                onClick={() => handleDotClick(index)} // Add onClick handler to change slide on dot click
              ></div>
            );
          })}
        </div>
      </div>
    </>
  );
};

export default LoggingCard;

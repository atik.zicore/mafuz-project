import React, { useState } from "react";
import { NavLink, useNavigate } from "react-router-dom";
import axios from "axios";
import { setCookie } from "../../Session";
import { toast } from "react-toastify";

const LoginForm = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const navigate = useNavigate();

  const handleLogin = async (e) => {
    e.preventDefault();
    axios
      .post("http://54.199.157.33/auth/login", {
        email,
        password,
      })
      .then(function (response) {
        // console.log(response);
        toast.success(" Login Successful", {
          position: "top-right",
          autoClose: 2000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          theme: "light",
        });
        setCookie("refresh_token_user", response.data.refresh_token);
        setCookie("access_token_user", response.data.access_token);
        if (response.data.user_type === "admin") {
          navigate("/admindashboard");
        } else {
          navigate("/userdashboard");
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  return (
    <div className="w-full xl:w-1/2 p-8 relative h-full">
      <h2 className="text-3xl text-center py-4">Login</h2>

      <form className="" onSubmit={handleLogin}>
        <div className="w-full mb-2 pt-8 space-y-2 sm:space-y-4 lg:space-y-8">
          <div className="relative z-0 p">
            <input
              type="email"
              id="login_email"
              className="block py-2.5 px-0 w-full text-base text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none focus:outline-none focus:ring-0 focus:border-secondary peer"
              placeholder=" "
              onChange={(e) => setEmail(e.target.value)}
              value={email}
              required
            />
            <label
              htmlFor="login_email"
              className="absolute text-base text-gray-500 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:start-0 peer-focus:text-secondary peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
            >
              Enter email address
            </label>
          </div>
          <div className="relative z-0 p">
            <input
              type="password"
              id="login_pass"
              className="block py-2.5 px-0 w-full text-base text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none focus:outline-none focus:ring-0 focus:border-secondary peer"
              placeholder=" "
              onChange={(e) => setPassword(e.target.value)}
              value={password}
              required
            />
            <label
              htmlFor="login_pass"
              className="absolute text-base text-gray-500 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:start-0 peer-focus:text-secondary peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
            >
              Enter Password
            </label>
          </div>
          <div>
            <input
              type="submit"
              className="w-full bg-[#0A2441] rounded-lg py-2 text-white font-light cursor-pointer"
            />
          </div>
        </div>
      </form>

      <div className="pt-6">
        <p className="text-gray-500 text-base">
          Don't have an account?{" "}
          <NavLink to="/signup" className="underline text-secondary capitalize">
            Sign up here
          </NavLink>
        </p>
      </div>
    </div>
  );
};

export default LoginForm;

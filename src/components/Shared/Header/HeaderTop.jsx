import React from 'react'
import { CiSearch } from 'react-icons/ci'
import { IoIosNotificationsOutline } from 'react-icons/io'
import { IoMoonOutline } from 'react-icons/io5'

const HeaderTop = () => {
  return (
    <div>
         <section className="w-full h-20 bg-white flex justify-between px-8 py-4 ">
          <div className="flex items-center h-full">
            <CiSearch className="w-8 h-8" />
          </div>
          <div className="flex justify-center items-center gap-x-4">
            <IoMoonOutline className="w-6 h-6" />
            <IoIosNotificationsOutline className="w-6 h-6" />
            <img
              src="/Images/one.svg"
              alt="user profile"
              className="w-10 h-10  object-contain bg-emerald-500 rounded-full"
            />
          </div>
        </section>
    </div>
  )
}

export default HeaderTop
import React from "react";
import { NavLink } from "react-router-dom";

const AdminHeader = () => {
  const navigation = [
    {
      name: "Home",
      path: "/AdminDashboard",
      // icon: <IoLayersOutline className="w-7 h-7" />,
      current: true,
    },
    {
      name: "Add Case",
      path: "/addCase",
      // icon: <HiOutlineClipboardDocumentList className="w-7 h-7" />,
      current: false,
    },
    {
      name: "Profile",
      path: "/",
      // icon: <MdOutlineBarChart className="w-7 h-7" />,
      current: false,
    },
  ];
  return (
    <div className="w-full h-full bg-[#2c3e50]">
      <div className="w-full flex justify-between items-center p-4 md:px-8 lg:px-12 xl:px-16 text-white">
        <div>
          <h2>LOGO</h2>
        </div>
        <div>
          <ul className="flex gap-4 cursor-pointer">
            {navigation.map((item, index) => {
              return <li key={index}>
                <NavLink to={item.path}>
                {item.name}
                </NavLink>
              </li>;
            })}
          </ul>
        </div>
      </div>
    </div>
  );
};

export default AdminHeader;

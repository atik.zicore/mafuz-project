import axios from "axios";
import React from "react";
import { NavLink, useNavigate } from "react-router-dom";
import Cookies from "js-cookie";
import { toast } from "react-toastify";

const UserHeader = () => {
  const navigate = useNavigate();
  const navigation = [
    {
      name: "Home",
      path: "/UserDashboard",
      // icon: <IoLayersOutline className="w-7 h-7" />,
      current: true,
    },

    {
      name: "Profile",
      path: "/UserDashboard",
      // icon: <MdOutlineBarChart className="w-7 h-7" />,
      current: false,
    },
  ];
  const baseURL = "http://54.199.157.33/";
  let authTokens = Cookies.get("access_token_user")
    ? Cookies.get("access_token_user")
    : null;
  let refreshTokens = Cookies.get("refresh_token_user")
    ? Cookies.get("refresh_token_user")
    : null;

  const handleLogout = () => {
    axios
      .post(
        baseURL + "auth/logout/",
        {
          refresh_token: refreshTokens,
        },

        {
          headers: {
            Authorization: `Bearer ${authTokens}`,
            "Content-Type": "application/json",
            accept: "application/json",
          },
        }
      )
      .then((res) => {
        navigate("/");
        toast.success(" logout Successful", {
          position: "top-right",
          autoClose: 2000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          theme: "light",
        });
      })

      .catch((err) => {
        // console.log(err);
      });
    Cookies.remove("access_token_user");
    Cookies.remove("refresh_token_user");
  };
  return (
    <div className="w-full h-full bg-[#2c3e50]">
      <div className="w-full flex justify-between items-center p-4 md:px-8 lg:px-12 xl:px-16 text-white">
        <div>
          <h2>LOGO</h2>
        </div>
        <div>
          <ul className="flex gap-4 cursor-pointer">


            <li className="" onClick={() => handleLogout()}>
              Logout
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
};

export default UserHeader;

import axios from "axios";
import dayjs from "dayjs";
import Cookies from "js-cookie";
import { jwtDecode } from "jwt-decode";

const baseURL = "http://54.199.157.33/";

let authTokens = Cookies.get("access_token_user")
  ? Cookies.get("access_token_user")
  : null;

const axiosInstance = axios.create({
  baseURL: baseURL,
  timeout: 1000,
  headers: {
    Authorization: `Bearer ${authTokens}`,
    "Content-Type": "application/json",
    accept: "application/json",
    // "Access-Control-Allow-Origin": "https://beta.satarko.com/",
  },
});

axiosInstance.interceptors.request.use(async (req) => {
  if (!authTokens) {
    authTokens = Cookies.get("access_token_user")
      ? Cookies.get("access_token_user")
      : null;
    req.headers.Authorization = `Bearer ${authTokens}`;
  }

  const user = { jwtDecode }(authTokens);
  const isExpired = dayjs.unix(user.exp).diff(dayjs()) < 1;

  if (!isExpired) return req;

  const response = await axios.post(`${baseURL}/auth/token_refresh/`, {
    refresh: Cookies.get("refresh_token_user"),
  });

  // console.log(response, "from interceptor");

  Cookies.set("refresh_token_user", response.data.refresh_token);
  Cookies.set("access_token_user", response.data.access_token);
  req.headers.Authorization = `Bearer ${response.data.access_token}`;
  return req;
});

export default axiosInstance;
